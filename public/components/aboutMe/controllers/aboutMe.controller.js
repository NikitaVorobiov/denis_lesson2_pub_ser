/**
 * Created by hanni on 11/10/16.
 */
(function () {

    angular
        .module('notebook')
        .controller('AboutMeController', [AboutMeController]);

    function AboutMeController() {
        console.log('About Me Controller');
    }
}());