/**
 * Created by hanni on 11/10/16.
 */
(function () {

    angular
        .module('notebook')
        .controller('AppController', [AppController]);

    function AppController() {
        console.log('App controller');
    }
}());