/**
 * Created by hanni on 11/10/16.
 */
(function () {

    angular
        .module('notebook')
        .controller('addNewContactController', ['$http', '$state', addNewContactController]);

    function addNewContactController($http, $state) {

        var self = this;
        self.contactName = '';
        self.contactNumber = '';
        self.contactAddress = '';

        self.addNewContact = function addNewContact() {
            //inspection for null text, title, author area
            if (self.contactName.length < 1 || self.contactNumber.length < 1 || self.contactAddress.length < 1) {
                alert('Vstav text');
                return;
            }
            //create  object for send to server
            var obj = {
                name: self.contactName,
                number: self.contactNumber,
                address: self.contactAddress
            };

            $http.post('/contacts', obj)
                .then(function success(res) {
                    //Reset variables
                    $state.go('app.contacts');
                }, function error(err) {
                    console.log(err);
                });
        }
    }
}());