/**
 * Created by hanni on 11/10/16.
 */
(function () {

    angular
        .module('notebook')
        .controller('ContactsDetailedInformationController', ['$stateParams', '$http', '$state', ContactsDetailedInformationController]);

    function ContactsDetailedInformationController($stateParams, $http, $state) {
        var self = this;
        self.contact = {};

        self.nameText = '';
        self.numberText = '';
        self.addressText = '';

        console.log($stateParams.id);

        self.getPostById = function getPostById(){
            //inspection for null idPost area
            if ($stateParams.id.length < 1 && !$stateParams.id) {
                alert('Input value');
                return null;
            }

            $http.get('/contacts/' + $stateParams.id)
                .then(function success(res) {
                    self.contact = res.data;

                    self.nameText = res.data.name;
                    self.numberText = res.data.number;
                    self.addressText = res.data.address;

                }, function error(err) {
                    console.log('err');
                });
        };

        self.getPostById();

        self.updateContact = function updateContact() {
            if (self.nameText.length < 1 || self.numberText.length < 1 || self.addressText.length < 1) {
                alert('Enter values!');
                return null;
            }

            var obj = {
                name: self.nameText,
                number: self.numberText,
                address: self.addressText
            };

            $http.put('/contacts/' + $stateParams.id, obj)
                .then(function success(res) {
                    //update  what we need
                    self.contact.name = obj.name;
                    self.contact.number = obj.number;
                    self.contact.address = obj.address;

                }, function error(err) {
                    console.log(err);
                });
        };

        self.deleteContact = function deleteContact(idContact) {
            //send id of contact to delete him
            $http.delete('/contacts/' + $stateParams.id)
                .then( function success(res) {

                    //call GET all posts function
                    $state.go('app.contacts');
                }, function error(err) {
                    console.log(err);
                });

        };

    }
}());