/**
 * Created by hanni on 11/10/16.
 */
(function () {


    angular
        .module('notebook')
        .controller('ContactsController', ['$http', '$state', ContactsController]);

    function ContactsController($http, $state) {
        var self = this;

        self.contacts = [];

        self.getAllContacts = function getAllContacts() {
            $http.get('/contacts')
                .then(function success(res) {
                    //transmiting data to our array of contacts
                    self.contacts = res.data;
                }, function error(err) {
                    console.log(err);
                });
        };

        //call GET all contacts function when the page opens
        self.getAllContacts();

        self.viewMoreContact = function viewMoreContact(idContact) {
            $state.go('app.contacts.detailedInformation', {id: idContact});
        };

        self.deleteContact = function deleteContact(idContact, index) {
                //send id of contact to delete him
                $http.delete('/contacts/' + idContact)
                    .then( function success(res) {

                        //call GET all posts function
                        self.contacts.splice(index, 1);

                    }, function error(err) {
                        console.log(err);
                    });

        };


    }
}());