/**
 * Created by hanni on 11/10/16.
 */
(function () {

    angular
        .module('notebook')
        .config(routes);

    function routes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state({
                name: 'app',
                url: '/app',
                controller: 'AppController',
                controllerAs: 'app',
                templateUrl: 'components/app/templates/app.template.html'
            })
            .state({
                name: 'app.aboutMe',
                url: '/aboutme',
                controller: 'AboutMeController',
                controllerAs: 'aboutMe',
                templateUrl: 'components/aboutMe/templates/aboutMe.template.html'
            })
            .state({
                name: 'app.contacts',
                url: '/contacts',
                controller: 'ContactsController',
                controllerAs: 'contacts',
                templateUrl: 'components/contacts/templates/contacts.template.html'
            })
            .state({
                name: 'app.contacts.detailedInformation',
                url: '/:id',
                controller: 'ContactsDetailedInformationController',
                controllerAs: 'contact',
                templateUrl: 'components/contacts/templates/contacts-detailedInformation.template.html'
            })
            .state({
                name: 'app.addNewContact',
                url: '/add',
                controller: 'addNewContactController',
                controllerAs: 'add',
                templateUrl: 'components/contacts/templates/contacts-addNewContact.template.html'
            });

        $urlRouterProvider.otherwise('app');

    }

}());