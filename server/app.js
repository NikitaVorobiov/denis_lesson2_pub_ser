/**
 * Created by hanni on 11/9/16.
 */
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(express.static(__dirname + '/../public'));

//rerouting request
require('./controllers/entry').routes(app);

//server init
var server = app.listen('3333', function () {
    console.log('Server port: ' + server.address().port);
});