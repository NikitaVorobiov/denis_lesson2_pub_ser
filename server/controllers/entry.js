/**
 * Created by hanni on 11/9/16.
 */
exports.routes = function (app) {
    var contacts = require('../data/contacts');

    // fetching all contact
    // return array of contacts
    app.get('/contacts', contacts.getAllContacts);

    // choice one contact by id
    // return object with contact information
    app.get('/contacts/:id', contacts.getContactById);

    // add new object in our data
    app.post('/contacts', contacts.addNewContact);

    //update contact by id
    app.put('/contacts/:id', contacts.updateContact);

    //delete contact by id
    app.delete('/contacts/:id', contacts.deleteContact);
};