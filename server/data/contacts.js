/**
 * Created by hanni on 11/9/16.
 */

var Uuid = require('node-uuid');

var data = [
    {
        id: '2',
        name: 'Nikita',
        number: '07984555',
        address:'Albishoara'
    },
    {
        id: '3',
        name:'Babka nura',
        number: '14488',
        address: 'tolkan 23/1'
    }
];

exports.getAllContacts = function getAllContacts(req, res) {
    res.send(data);
};

exports.getContactById = function getContactById(req, res) {

    var ver = 0;
    //choose the right contact from array
    data.forEach( function (value) {
        if (value.id == req.params.id) {
            res.send(value);
            ver += 1;
        }
    });
    if (ver == 0) {
        res.send(req.params.id + ' is not exist');
    }
};

exports.addNewContact = function addNewContact(req, res) {
    if (!req.body.name) {
        res.status(409).send('Name is mandatory');
        return null;
    }
    if (!req.body.number) {
        res.status(409).send('Number is mandatory');
        return null;
    }
    if (!req.body.address) {
        res.status(409).send('Address is mandatory');
        return null;
    }

    var id = Uuid.v4();
    //create object for adding in contacts
    var obj = {
        id: id,
        name: req.body.name,
        number: req.body.number,
        address: req.body.address,
    };

    //Adding object in array
    data.push(obj);

    res.send('Done');

};

exports.updateContact = function updateContact(req, res) {
    if (!req.body.name) {
        res.status(409).send('Name is mandatory');
        return null;
    }
    if (!req.body.number) {
        res.status(409).send('Number is mandatory');
        return null;
    }
    if (!req.body.address) {
        res.status(409).send('Address is mandatory');
        return null;
    }

    var ver = 0;

    //rewrite required object
    data.forEach( function (value) {
        if (value.id == req.params.id) {

            value.name = req.body.name;
            value.number = req.body.number;
            value.address = req.body.address;

            ver += 1;
            res.send('Done');
            return null;
        }
    });
    if (ver == 0) {
        res.send(req.params.id + ' is not exist');
    }

};

exports.deleteContact = function deleteContact(req, res) {

    var ver = 0;
    var indexForRemove = -1;
    data.forEach( function (value, index) {
        if (value.id == req.params.id) {
            indexForRemove = index;
            ver += 1;
        }
    });

    if (indexForRemove != -1) {
        data.splice(indexForRemove, 1);
        res.send('Done');
        return null;
    }
    if (ver == 0) {
        res.send(req.params.id + ' is not exist');
    }

};


